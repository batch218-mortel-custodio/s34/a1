// This code will help us access contents of express module/package
	// A "module" is a software component or part of a program that contains one or more routines
// It also allows us to access methods and functions that we will use to easily create an app/server
// We store oure express module to variable so we could easily access its keywords, functions, and methods.
const express = require("express");

// This code creates an application using express / a.k.a express application
	// App is our server
const app = express();

const port = 3000;

app.use(express.json());


// allows your app to read data from forms
// by default, info received from the url can only be received as string or an array
// by appling the option of "extended true", this allows us to receive info  in other data types, such as an object which we will use throughout our application 
app.use(express.urlencoded({extended:true}));


// this route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response)=>{
	// this is the repsonse that we will expect to receive if the get method with the right endpoint is successfuk
response.send("GET method success. \nHello from  /hello endpoint!");
});

app.post("/hello", (request, response)=>{
	// this is the repsonse that we will expect to receive if the get method with the right endpoint is successfuk
response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});


let users = [];

app.post("/signup", (request, response) => {
	if (request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		console.log(users);
		response.send(`User ${request.body.username} successfully registered!`);
	}
	else{
		response.send("Please input BOTH username and password, k?");
	}
});


app.put("/change-password", (request, response) => {
	let message;

	// checks all usernames found in the array
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;
			message  = `User ${request.body.username}'s password has been updated`;
			break;
		}
		else{
			message = "User does not exist.";
		}
	}
	console.log(users);
	response.send(message);
});

// 1
// create a route that expects GET request at the URI/endpoint "/home"
// response that will be received should be "Welcome to the homepage"
// Code below...

app.get("/home", (request, response)=>{

response.send("Welcome to the homepage");
});


// 2
// create a route that expects GET request at the URI/endpoint "/users"
// response that will be received IS a COLLECTION OF USERS
// Code below..


app.get("/users", (request, response)=>{

response.send(users);
});


// 3
// create a route that expects DELETE request at the URI/endpoint "/delete-user"
// the program SHOULD CHECK IF THE USER is existing before deleting
// Response that will be received is the updated collection of users

app.delete("/delete-user", (request, response)=>{
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i, 1);
			break;
		}
		else{
			response.send("User does not exist.");
		}
	};
	response.send(users);
});



// 4
// save all the successful request tabs and EXPORT the collection
// UPLOAD/UPDATE your GITLAB repo







// Tells our server/application to listen to the port
// if the port is accessed, we can run the server
// returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server running at port ${port}.`));





